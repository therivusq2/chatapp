# Chat Application

2-way encrypted online chat application written in Java programming language. This is the exam project of our team.

# Contact US

- Địa chỉ: Đỗ Xuân Hợp, An Phú, TP Thủ Đức, TP HCM
- Hotline: 0938.279.155
- Email: therivusquan2@gmail.com
- Website: [https://theglobalcitys.com.vn/](https://theglobalcitys.com.vn/)
